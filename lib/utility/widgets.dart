import 'package:flutter/material.dart';

InputDecoration buildInputdecoration(String hintText, IconData icon) {
  return InputDecoration(
      prefixIcon: Icon(icon, color: const Color.fromRGBO(50, 62, 72, 1.0)),
      contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)));
}

MaterialButton longButton(String title, Function fun,
    {Color color = Colors.blue, Color textColor = Colors.white}) {
  return MaterialButton(
    onPressed: () {
      // ignore: avoid_print
      print("Hello click");
    },
    textColor: textColor,
    color: color,
    // ignore: sort_child_properties_last
    child: SizedBox(
      width: double.infinity,
      child: Text(
        title,
        textAlign: TextAlign.center,
      ),
    ),
    height: 45,
    minWidth: 600,
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10))),
  );
}
