// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

import '../utility/widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late String username, password;

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_function_declarations_over_variables
    var doLogin = () {};

    final forgotLabel = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
            padding: const EdgeInsets.all(0.0),
            child: const Text(
              "Forgot password?",
              style: TextStyle(fontWeight: FontWeight.w300),
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/reset-password');
            }),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(40.0),
          child: Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 15.0),
                const Text('Email'),
                const SizedBox(
                  height: 5.0,
                ),
                TextFormField(
                  autofocus: false,
                  onSaved: (value) => username = value!,
                  decoration: buildInputdecoration('Enter email', Icons.email),
                ),
                const SizedBox(height: 15.0),
                const Text('Password'),
                const SizedBox(height: 5.0),
                TextFormField(
                  autofocus: false,
                  obscureText: true,
                  onSaved: (value) => password = value!,
                  decoration:
                      buildInputdecoration('Enter password', Icons.lock),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                longButton('Login', doLogin),
                const SizedBox(height: 8.0),
                forgotLabel
              ],
            ),
          ),
        ),
      ),
    );
  }
}
